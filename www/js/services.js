angular.module('apps')

.factory('authService', function ($http, $location, $ionicHistory) {

	var url = 'http://jasasoftware.co/sekolah/dev3/index.php/api';

	return{
		//function check authentication
		authenticated : function (auth) {
			return $http.post(url + '/auth', auth, {
				headers : {
					'Content-Type':'application/x-www-form-urlencoded;charset=UTF-8;'
				}
			})
		},
		//function set localstorage when login
		setUser : function(user_id, names, email, password, position, cabang_id){
			localStorage.setItem("id",user_id);
			localStorage.setItem("name",names);
			localStorage.setItem("email",email);
			localStorage.setItem("password",password);
			localStorage.setItem("position",position);
			localStorage.setItem("cabang_id",cabang_id);
		},
		//function remove localstorage when logout
		removeUser : function(){
			localStorage.removeItem("id");
			localStorage.removeItem("name");
			localStorage.removeItem("email");
			localStorage.removeItem("password");
			localStorage.removeItem("position");
			localStorage.removeItem("cabang_id");
			localStorage.removeItem("t_absen");
			localStorage.removeItem("t_jurnal");

			$ionicHistory.clearCache();
			$ionicHistory.clearHistory();

		}
	}

})

.factory('absenService', function ($http) {

	var url = 'http://jasasoftware.co/sekolah/dev3/index.php/api';

	return {
		//function get list cabang absen
		getCabang : function (response) {
			return $http.get(url + '/getcabang');
		},
		//function get list jadwal absensi filter by teacher
		getClass : function (id, teach) {
			return $http.get(url + '/classchedule/' + id + '/' + teach);
		},
		//function get data siswa by id class
		getSiswa : function (id) {
			return $http.get(url + '/student/' + id)
		},
		//function post data absensi
		postsAbsen : function (attendance) {
			return $http.post(url + '/savestudent', attendance,{
				headers : {
					'Content-Type':'application/x-www-form-urlencoded;charset=UTF-8;'
				}
			})
		}
	};
})

.factory('jurnalService', function ($http) {

	var url = 'http://jasasoftware.co/sekolah/dev3/index.php/api';
	
	return {
		//function get list cabang jurnal
		getCabang : function (response) {
			return $http.get(url + '/getcabang');
		},
		//function get list jadwal jurnal filter by teacher
		getClass : function (id, teach) {
			return $http.get(url + '/classchedule/' + id + '/' + teach);
		},

		getJurnal : function () {
			return $http.get(url + '/jurnal/' + 2);
		},
		//function post data jurnal
		saveJurnal : function (jurnalObj) {
			return $http.post(url + '/savejurnal',jurnalObj, {
				headers:{
					'Content-Type':'application/x-www-form-urlencoded;charset=UTF-8;'
				}
			})
		},

		updateJurnal : function (id, jurnalDetail) {
			return $http.post(url + '/updatejurnal/' + id, jurnalDetail,{
				headers:{
					'Content-Type':'application/x-www-form-urlencoded;charset=UTF-8;'
				}
			})
		},

		editJurnal : function (id) {
			return $http.get(url + '/jurnal/edit/' + id);
		},

		deleteJurnal : function (id) {
			return $http.delete(url + '/deletejurnal/' + id);
		}
	};
})