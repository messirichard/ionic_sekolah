angular.module('apps', ['ionic','ngCookies','ngRoute','checklist-model'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('app', {
      url: '/app',
      abstract: true,
      templateUrl: 'templates/menu.html'
    })
    //route home
    .state('app.home', {
      url: '/home',
      views: {
        'menuContent': {
          templateUrl: 'templates/home.html',
          controller:'homeCtrl'
        }
      }
    })
    //route absensi percabang
    .state('app.absen-cabang', {
      url: '/absen-cabang',
      views: {
        'menuContent': {
          templateUrl: 'templates/cabang/absen-cabang.html',
          controller: 'absenCtrl'
        }
      }
    })
    //route absensi perkelas
    .state('app.absen-kelas',{
      url:'/absen-kelas/:id',
      views:{
        'menuContent':{
          templateUrl:'templates/kelas/absen-kelas.html',
          controller: 'absenCtrl'
        }
      }
    })
    //route absensi
    .state('app.absensi',{
      url:'/absensi/:id',
      views:{
        'menuContent':{
          templateUrl:'templates/absen/absensi.html',
          controller:'absenCtrl'
        }
      }
    })
    //route jurnal percabang
    .state('app.jurnal-cabang', {
      url: '/jurnal-cabang',
      views: {
        'menuContent': {
          templateUrl: 'templates/cabang/jurnal-cabang.html',
          controller: 'jurnalCtrl'
        }
      }
    })
    //route jurnal perkelas
    .state('app.jurnal-kelas',{
      url:'/jurnal-kelas/:id',
      views:{
        'menuContent':{
          templateUrl:'templates/kelas/jurnal-kelas.html',
          controller: 'jurnalCtrl'
        }
      }
    })
    //route jurnal
    .state('app.jurnal',{
      url:'/jurnal/:id',
      views:{
        'menuContent':{
          templateUrl:'templates/jurnal/jurnals.html',
          controller:'jurnalCtrl'
        }
      }
    })
    //route aproval kepsek
    .state('app.aprovals',{
      url:'/aprovals',
      views:{
        'menuContent':{
          templateUrl:'templates/aproval/aprovals.html',
          controller:'aprovCtrl'
        }
      }
    })
    //route aproval kepsek
    .state('app.aproval',{
      url:'/aproval',
      views:{
        'menuContent':{
          templateUrl:'templates/aproval/aproval.html',
          controller:'aprovCtrl'
        }
      }
    })


    //route login
    .state('login',{
      url:'/login',
      templateUrl:'templates/auth/login.html',
      controller:'authCtrl'
      }
    )

    $urlRouterProvider.otherwise('/app/home');

})
//root of route / auth
.run(function ($rootScope, $location, $cookieStore, $http, $state, authService) {

  if('name' && 'password' in localStorage){
      // $location.path('/app/home');
  }else{
    authService.removeUser(); 
    $location.path('/login');
  }
});