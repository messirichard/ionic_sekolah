angular.module('apps')

.controller('authCtrl', function (authService, $scope, $location, $timeout, $ionicPopup, $ionicHistory, $window) {

	$scope.auth = {};
	//function login
	$scope.login = function(){
		authService.authenticated($scope.auth).success(function(response){
			//initialisasi variable + set data user when login function running
			var id = response.id;
			var names = response.username;
			var email = response.email;
			var password = response.password;
			var position = response.position;
			var cabang_id = response.cabang_id;
			//set user localstorage
			authService.setUser(id, names, email, password, position, cabang_id);
			$location.path('/app/home');
			//clear input form
			$scope.auth.username = '';
			$scope.auth.password = '';

		}).error(function(response){
			//alert auth when lost
			$scope.alertlost();
			//clear input form
			$scope.auth.username = '';
			$scope.auth.password = '';
		});
	}
	//function logout
	$scope.logout = function(){
		$window.location.reload();
		authService.removeUser();
		$ionicHistory.clearCache();
		$ionicHistory.clearHistory();
		$location.path('/login');
	}
	//alert lost
	$scope.alertlost = function() {
	   	$ionicPopup.alert({
	   		title: 'Information !',
	    	template: "<center><p>Email and Password incorrect</p></center>"
	   	});
	}


})

.controller('homeCtrl', function($scope, $http, $timeout, $ionicPopup,$ionicModal, dateFilter){
	//check status user
	var position = localStorage.getItem('position');
	if (position == "Teacher") {
		$scope.status_t  = true;
		$scope.status_h  = false;
		$scope.status_ho = false;
	}else if (position == "Head Office"){
		$scope.status_ho  = true;
		$scope.status_t  = false;
		$scope.status_h  = false;
	}else{
		$scope.status_h  = true;
		$scope.status_t  = false;
		$scope.status_ho = false;
	}

	var url = 'http://jasasoftware.co/sekolah/dev3/index.php/api';

	var data_notice = [];
	//function get notification
	$http.get(url + '/getNotice').success(function(response){
		$scope.notive = response.content;

		$scope.subject = $scope.notive[0].subject;
		$scope.notice  = $scope.notive[0].notice;

		data_notice.push({
			a : $scope.subject,
			b : $scope.notice
		})
	})

	console.log(localStorage);
	//alert notification
	$scope.info = function run(){
		$scope.checked = {};
		var myPopup = $ionicPopup.show({
		    title: '<strong><i class=ion-speakerphone></i> INFORMATION</strong>',
		    templateUrl: 'templates/notive/notive.html',
		    scope: $scope,
		    buttons: [
		      {
		        text: '<b><i class=ion-checkmark></i> Ok</b>',
		        type: 'button-positive',
		        attr: 'data-ng-disabled="!checked.notice"',
		        onTap: function(e) {
		          if (!$scope.checked.notice) {
		            e.preventDefault();
		            $scope.showError = true;
		          } else {
		            return $scope.checked.notice;
		          }
		        }
		      },
		    ]
		});

		myPopup.then(function(res) {
		    
		});
	}
	//alert information
	$scope.alertInfo = function() {
	   	var alertPopup = $ionicPopup.show({
	    	title: '<strong><i class=ion-speakerphone></i> INFORMATION</strong>',
	     	template: '<center><strong>Tidak Ada Pengumuman untuk hari ini !</strong></center>'
	   	});

	   	$timeout(function() {
	   	    alertPopup.close();
	   	}, 5000);

	}

})

.controller('jurnalCtrl', function(jurnalService, $rootScope, $scope, $stateParams, $state, $location, $ionicPopup, $timeout, $window){
	//get text list class
	$scope.getText = function(event, id){
	    localStorage.setItem('t_jurnal', id);
	}
	//get cabang 
	jurnalService.getCabang().success(function (data) {
		$scope.cabang = data.content;
	})
	//get cabang id from localstorage
	var id_cabang = localStorage.getItem('cabang_id');
	$scope.cabangId = {
	    cabang_id: id_cabang
	}
	//get name from localstorage
	var user = localStorage.getItem('name');
	//get class by id
	jurnalService.getClass($stateParams.id, user).success(function (data) {
		$scope.kelas = data.content;
	})

	//initialisasi variable localstorage
	var id_user = localStorage.getItem('id');
	var user = localStorage.getItem('name');
	var id_class = localStorage.getItem('t_jurnal');
	//initialisasi variable data post
	$scope.user = user;
 	$scope.user_id = id_user;
	$scope.jurnalObj = {
		class_id : id_class,
		task : $scope.task,
		situasi_kelas : $scope.situasi_kelas,
		catatan : $scope.catatan,
		user : $scope.user,
		user_id : $scope.user_id
	};
	//function check form input jurnal
	$scope.simpanjurnal = function () {

		if (!$scope.jurnalObj.task && !$scope.jurnalObj.task) {
			$scope.alertInfo();
		}else if (!$scope.jurnalObj.situasi_kelas && !$scope.jurnalObj.situasi_kelas) {
			$scope.alertInfo();
		}else if (!$scope.jurnalObj.catatan && !$scope.jurnalObj.catatan ) {
			$scope.alertInfo();
		}else{
			$scope.showPopup();
		}
	}
	//function post data jurnal
	$scope.data = function () {
		jurnalService.saveJurnal($scope.jurnalObj).success(function (response) {
			$scope.alertSuccess();
			// clear form input
			$scope.jurnalObj.class_id = '';
			$scope.jurnalObj.task = '';
			$scope.jurnalObj.situasi_kelas = '';
			$scope.jurnalObj.catatan = '';
		})
	}
	//alert confirmation
	$scope.showPopup = function() {
	  		var confirm = $ionicPopup.confirm({
			    title: 'Simpan Jurnal ?',
			    template: '<center>Apakah anda sudah yakin akan menyimpan data ini ?</center>'
			});

			confirm.then(function (res) {
				if(res) {
			       $scope.data();
			    } else {
			       confirm.close();
			    }
			})
	    }
	//alert when success
	$scope.alertSuccess = function() {
	   	var alertPopup = $ionicPopup.show({
	    	title: 'Data Tersimpan !',
	     	template: '<center><img src="img/ok.png"></center>'
	   	});

	   	$timeout(function() {
	   	    alertPopup.close();
	   	    $window.location.reload();
	   	    $location.path('/app/home');
	   	}, 1500);

	}
	//alert warning info
	$scope.alertInfo = function() {
	   	alertPopup = $ionicPopup.show({
	    	title: 'Warning !',
	     	template: '<center><h4>Salah satu text masih kosong, harap diisi !</h4></center>',
	     	buttons: [
	     	    {
	     	        text: '<button><i class="ion-ios-close-outline"></i> Close</button>',
	     	        type: 'button-assertive'
	     	    },
	     	]
	   	});
	}

})

.controller('absenCtrl', function(absenService, $http, $scope, $stateParams, $ionicPopup, $timeout, $location, $window){
	//function get cabang 
	absenService.getCabang().success(function (data) {
		$scope.cabang = data.content;
	})
	//function get cabang id from localstorage
	var id_cabang = localStorage.getItem('cabang_id');
	$scope.cabangId = {
	    cabang_id: id_cabang
	}
	//set user name
	var user = localStorage.getItem('name');
	//function get class by id + username
	absenService.getClass($stateParams.id, user).success(function (data) {
		console.log(data);
		$scope.kelas = data.content;
	})

	var url = 'http://jasasoftware.co/sekolah/dev3/index.php/api';
	//get student by id class
	$http.get(url + /student/ + $stateParams.id).success(function (data) {
		$scope.siswa = data.content;
			//get text class
			$scope.getText = function(event){
	            var txt = event.target.innerHTML;
	            localStorage.setItem('t_absen', txt);
	        }
			// initialiasasi variable data
			var attendance = [];
			$scope.attendance = {};
			var temp = localStorage.getItem('t_absen');
			$scope.class = temp;
			//looping data absensi before save
			for (var i = 0; i < $scope.siswa.length; i++) {
				var roll_;
				var atudentName_;
				var userId_;
				var studentInfoId_;
				var in_velu;
				//push data student to attendance array
				attendance.push([
					{key:'classTitle',value:$scope.class},
					{key:'roll_' + $scope.siswa[i].roll_number, value: $scope.siswa[i].roll_number},
					{key:'atudentName_' + $scope.siswa[i].roll_number, value:$scope.siswa[i].student_nam},
					{key:'action_' + $scope.siswa[i].roll_number,value:"A"},
					{key:'userId_' + $scope.siswa[i].roll_number, value:$scope.siswa[i].user_id},
					{key:'studentInfoId_' + $scope.siswa[i].roll_number , value:$scope.siswa[i].student_id},
					{key:'in_velu', value: $scope.siswa[i].id}
				]);
			}			
			//looping data attendance
			for(var i = 0; i < attendance.length; i++){
				for(var j = 0; j < 7; j++){
					$scope.attendance[attendance[i][j].key] = attendance[i][j].value;
				}
			}
			//function save absensi
			$scope.data = function () {
				absenService.postsAbsen($scope.attendance).success(function (response) {
					$scope.alertSuccess();

				}).error(function (response) {
					// console.log(response);
				})
			}
			//check confirmation
			$scope.saveAbsen = function () {
				$scope.showPopup();
			}

	})
	// alert confirmation
	$scope.showPopup = function() {
	  		var confirm = $ionicPopup.confirm({
			    title: 'Simpan Absensi ?',
			    template: '<center>Apakah anda sudah yakin akan menyimpan data ini ?</center>'
			});

			confirm.then(function (res) {
				if(res) {
			       $scope.data();
			    } else {
			       confirm.close();
			    }
			})
	    }
	// alert success
	$scope.alertSuccess = function() {
	   	var alertPopup = $ionicPopup.show({
	    	title: 'Data Tersimpan !',
	     	template: '<center><img src="img/ok.png"></center>'
	   	});

	   	$timeout(function() {
	   	    alertPopup.close();
	   	    $location.path('/app/home');
	   	}, 1500);

	}
	
})

.controller('aprovCtrl',function ($scope, $http, $ionicPopup, $location, $timeout, $window) {

	var url = 'http://jasasoftware.co/sekolah/dev3/index.php/api';
	// initialisasi variabel
	var id_cabang 	= localStorage.getItem('cabang_id');
	var position 	= localStorage.getItem('position');
	// get cabang
	$scope.cabangId = {
	    cabang_id: id_cabang
	}
	//initialisasi variabel
	$scope.aprov 	= [];
	$scope.aprov2 	= [];
	$scope.aprove 	= true;
	$scope.block 	= false;
	//get data un aproval
	$http.get( url + '/getUnappJurnal').success(function(response){
		$scope.aproval = response.content;

		$scope.mark = function (id, status) {
			var stts = 0;
			if (position == "Headmaster") {
				if(status){
					stts = 1;
				}else{
					stts = 2;
				}
			}else if (position == "Head Office") {
				if(status){
					stts = 3;
				}else{
					stts = 4;
				}
			}
			if($scope.aprov.includes(id)){
				$scope.aprov2[$scope.aprov.indexOf(id)] = stts;
			}else{
				$scope.aprov.push(id);
				$scope.aprov2.push(stts);
			}
		}
	})
	//clear all check
	$scope.clear = function () {
		$scope.aprov.splice(0);
		$scope.aprov2.splice(0);
	}

	// function update data aproval check
	$scope.update = function () {
		if ($scope.aprov.length !== 0 && $scope.aprov2.length !== 0) {
			$scope.showPopup();
		}else{
			$scope.alertDataNull();
		}
	}
	// function save aproval
	$scope.data = function () {
		if ($scope.aprov.length !== 0) {
			$http.post( url + '/updateapprovejurnal',
				{id:$scope.aprov,approve:$scope.aprov2},
				{
					headers :{'Content-Type':'application/x-www-form-urlencoded;charset=UTF-8;'}
				}
			).success(function (response) {
				$scope.alertSuccess();
			}).error(function (response) {
				$scope.alertInfo();
			})
		}else{
			$scope.alertInfo();
		}
	}
	// alert confirmation
	$scope.showPopup = function() {
	  	var confirm = $ionicPopup.confirm({
			title: 'Simpan Aproval ?',
			template: '<center>Apakah anda sudah yakin akan menyimpan data ini ?</center>'
		});

		confirm.then(function (res) {
			if(res) {
			    $scope.data();
			}else{
			    confirm.close();
			}
		})
	}
	// alert success
	$scope.alertSuccess = function() {
	   	var alertPopup = $ionicPopup.show({
	    	title: '<h4>Data Tersimpan !</h4>',
	     	template: '<center><img src="img/ok.png"></center>'
	   	});

	   	$timeout(function() {
	   	    alertPopup.close();
	   	    $window.location.reload();
	   	    $location.path('/app/home');
	   	}, 1500);
	}
	// alert info data lost
	$scope.alertInfo = function() {
	   	var alertPopup = $ionicPopup.show({
	    	title: '<h4>Data Gagal Disimpan !</h4>',
	     	template: '<center><img src="img/close.png"></center>'
	   	});

	   	$timeout(function() {
	   	    alertPopup.close();
	   	}, 1500);
	}
	// alert data info when null
	$scope.alertDataNull = function() {
	   	var alertPopup = $ionicPopup.show({
	    	title: '<h4 class=alertNullData>Information !</h4>',
	     	template: '<center><h5>Anda Belum Memilih<br>Salah  Satu Data Untuk Disimpan !</h5></center>'
	   	});

	   	$timeout(function() {
	   	    alertPopup.close();
	   	}, 1500);
	}

	
})